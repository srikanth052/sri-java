package com.sri.app.repository;

import com.sri.app.model.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailsRepositry extends JpaRepository<UserDetails, Integer> {
}
