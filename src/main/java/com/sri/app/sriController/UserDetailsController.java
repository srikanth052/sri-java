package com.sri.app.sriController;

import com.sri.app.model.User;
import com.sri.app.model.UserDetails;
import com.sri.app.repository.UserDetailsRepositry;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserDetailsController {

    WebDriver driver;

    @Autowired
    UserDetailsRepositry userDetailsRepositry;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    JdbcTemplate jdbcTemplate;



    @GetMapping("/users")
    public List<UserDetails> getAllUsers(){
        return userDetailsRepositry.findAll();
    }

    @PostMapping(value = "/load")
    public List<UserDetails> persist(@RequestBody final UserDetails users) {
        userDetailsRepositry.save(users);
        return userDetailsRepositry.findAll();
    }

    @GetMapping("/getUserDetails")
    public List<User> getUserDetails(){
        List<User> users = new ArrayList<>();


        System.setProperty("webdriver.chrome.driver", "/Users/srikanthtirumalasetti/Downloads/chromedriver");
        driver = new ChromeDriver();

        driver.get("https://secure.bankofamerica.com/login/sign-in/signOnV2Screen.go");

        WebElement loginId = driver.findElement(By.id("enterID-input"));

        WebElement password = driver.findElement(By.id("tlpvt-passcode-input"));

        loginId.sendKeys("srikanth052");
        password.sendKeys("P.nishok270");

        WebElement signIn = driver.findElement(By.name("enter-online-id-submit"));

        signIn.click();


//		String xpath = "//*[@id=\"VerifyCompForm\"]/div[2]/label";
//
//		WebElement securityQuestion = driver.findElement(By.xpath(xpath));
//
//
//		securityQuestion.getText();
//
//		System.out.println("securityQuestion" + securityQuestion.getText());
//
//
//		WebElement securityQuestionAnswerField = driver.findElement(By.id("tlpvt-challenge-answer"));
//
//		if ((securityQuestion.getText() == "What was the make and model of your first car?") ) {
//			securityQuestionAnswerField.sendKeys("ford");
//		} else if ("What was the name of your first boyfriend or girlfriend?" == securityQuestion.getText()) {
//			securityQuestionAnswerField.sendKeys("mounica");
//		} else {
//			driver.close();
//		}

        String xpathForBalance = "//*[@id=\"Traditional\"]/li/div[1]/div[1]/span";

        WebElement bofaBalance = driver.findElement(By.xpath(xpathForBalance));

        bofaBalance.getText();

        System.out.println("bofaBalance: " + bofaBalance.getText());

        /*if (bofaBalance.getText() != null ){*/
        String sql = "update sri.banks set mnm_pay_amount = ?  where id = 1";

        // jdbcTemplate.update(sql, bofaBalance.getText());

        this.instertBofaBlance(bofaBalance.getText().toString());
      /*  } else {
            System.out.println("bofaBalance not inserted" + bofaBalance.getText());
        }*/



        WebElement signOut = driver.findElement(By.name("onh_sign_off"));

        signOut.click();

        /*String sql = "select USERNAME as userName, EMAIL as email, FIRST_NAME as name from sri.user where id = 1";

        users = namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<User>(User.class));*/

        return users;
    }

    public void instertBofaBlance(String balance){

        String sql = "update sri.banks set balance = ?  where id = 1";

        jdbcTemplate.update(sql, balance);



    }


}
