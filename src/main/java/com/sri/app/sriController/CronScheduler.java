package com.sri.app.sriController;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class CronScheduler {

    @Autowired
    UserDetailsController userDetailsController;

    @Scheduled(cron = "0 06 23 ? * *")
    public void run() throws InterruptedException {
        System.out.println("Cron scheduler is running at " + new Date());
        userDetailsController.getUserDetails();
        Thread.sleep(3000);
    }

}
