package com.sri.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableJpaRepositories(basePackages = "com.sri.app.repository")
@SpringBootApplication
@EnableScheduling
public class sriApplication {

    public static void main(String [] args){
        SpringApplication.run(sriApplication.class, args);
    }

}
